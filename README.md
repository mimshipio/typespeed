# Installation
This program requires `textdistance` to run.

To install `textdistance` run `pip install textdistance`.
