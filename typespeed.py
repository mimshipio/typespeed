import random
import time
import textdistance
import curses

def draw(screen):
    screen = curses.initscr()
    curses.echo()
    screen.immedok(True)

    height, width = screen.getmaxyx()

    text = open("kjv.tsv").read().splitlines()

    k = 0 

    while (k != 1):
        line = random.choice(text)

        if len(str(line)) >= width:
            continue

        start_x_text = int((width // 2) - (len(line) // 2) - len(line) % 2)
        start_y = int((height // 2) - 2)

        screen.addstr(start_y,start_x_text, line)

        start = time.time()
        input = screen.getstr(start_y+2,start_x_text)
        u_line = str(input.decode())
        wc = len(u_line.split())
        end = time.time()
        
        min = (end - start)/60
        min_str = str(int(end - start))
        wpm = str(int(wc/min))
        dist = str(textdistance.ratcliff_obershelp.normalized_distance(line,u_line))

        stat_min = "Time:" + min_str
        stat_wpm = "WPM:" + wpm
        stat_dist = "Accuracey:" + dist

        start_x_min = int((width // 2) - (len(stat_min) // 2) - len(stat_min) % 2)
        start_x_wpm = int((width // 2) - (len(stat_wpm) // 2) - len(stat_wpm) % 2)
        start_x_dist = int((width // 2) - (len(stat_dist) // 2) - len(stat_dist) % 2)

        screen.clear()
        screen.addstr(start_y-7,start_x_min, stat_min)
        screen.addstr(start_y-6,start_x_wpm, stat_wpm)
        screen.addstr(start_y-5,start_x_dist, stat_dist)

def main():
    curses.wrapper(draw)

if __name__ == "__main__":
    main()
